<?php require_once "./code.php";?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h2>Divisible by 5</h2>
    <p><?= numbersByFive();?></p>

    <h1>Printing the names added so far in student array</h1>

    <pre><?php print_r($classmates); ?></pre>

    <h2>Count of the arrays</h2>
    <pre><?= count($classmates);?></pre>

    <h2>Added studne in the last index</h2>

    <?php array_push($classmates, 'Ian');?>
    <pre><?php print_r($classmates); ?></pre>

    <?php array_shift($classmates);?>

    <h2>Count of the arrays</h2>
    <pre><?= count($classmates);?></pre>

    <h2>Removed first student</h2>

    <pre><?php print_r($classmates); ?></pre>

</body>

</html>